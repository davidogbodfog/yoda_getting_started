README.txt

To build the starter YODA system using Maven:

> mvn package

To run the starter YODA system from the command line: 

> mvn exec:java -Dexec.mainClass="edu.cmu.sv.StarterCommandLineSystem"
