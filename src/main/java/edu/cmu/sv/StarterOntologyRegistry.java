package edu.cmu.sv;

import edu.cmu.sv.domain.OntologyRegistry;
import edu.cmu.sv.domain.ontology.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by David Cohen on 6/23/15.
 */
public class StarterOntologyRegistry implements OntologyRegistry{

    public Set<Verb> verbs = new HashSet<>();
    public Set<Role> roles = new HashSet<>();
    public Set<Noun> nouns = new HashSet<>();
    public Set<Quality> qualities = new HashSet<>();
    public Set<QualityDegree> qualityDegrees = new HashSet<>();

    @Override
    public Set<Verb> getVerbs() {
        return verbs;
    }

    @Override
    public Set<Role> getRoles() {
        return roles;
    }

    @Override
    public Set<Noun> getNouns() {
        return nouns;
    }

    @Override
    public Set<Quality> getQualities() {
        return qualities;
    }

    @Override
    public Set<QualityDegree> getQualityDegrees() {
        return qualityDegrees;
    }

    //TODO: DEFINE THE DOMAIN ONTOLOGY BELOW
    /*
    // define roles
    public static Role component = new Role("Component", false);

    // define nouns
    public static Noun appliance = new Noun("Appliance", YodaSkeletonOntologyRegistry.physicalNoun);

    // define quality groups
    public static Quality cleanliness = new Quality("Cleanliness", room, null,
            new ScaledShiftedSingleValueQueryFragment("dust_level", 0.0, 5.0, true));
    public static QualityDegree clean = new QualityDegree("Clean", 1.0, 2.0, cleanliness);
    public static QualityDegree dirty = new QualityDegree("Dirty", 0.0, 1.0, cleanliness);

    // define verbs
    public static Verb turnOnAppliance = new Verb("TurnOnAppliance", Arrays.asList(component), new LinkedList<>());

    // finalize role domains and ranges
    static {
        component.getDomain().addAll(Arrays.asList(turnOnAppliance, turnOffAppliance));
        component.getRange().addAll(Arrays.asList(appliance));
    }

    public StarterOntologyRegistry() {
        verbs.add(turnOnAppliance);

        nouns.add(appliance);

        roles.add(component);

        qualities.add(cleanliness);

        qualityDegrees.add(clean);
        qualityDegrees.add(dirty);
    }
    */

}
