package edu.cmu.sv;

import edu.cmu.sv.domain.DatabaseRegistry;
import edu.cmu.sv.domain.DomainSpec;
import edu.cmu.sv.domain.NonDialogTaskRegistry;
import edu.cmu.sv.domain.yoda_skeleton.YodaSkeletonLexicon;
import edu.cmu.sv.domain.yoda_skeleton.YodaSkeletonOntologyRegistry;
import edu.cmu.sv.yoda_environment.SubprocessYodaSystem;

import java.io.IOException;

/**
 * Created by David Cohen on 6/23/15.
 */
public class StarterSubprocessSystem extends SubprocessYodaSystem {
    static {
        // skeleton domain
        domainSpecs.add(new DomainSpec(
                "YODA skeleton domain",
                new YodaSkeletonLexicon(),
                new YodaSkeletonOntologyRegistry(),
                new NonDialogTaskRegistry(),
                new DatabaseRegistry()));
        // starter domain
        domainSpecs.add(new DomainSpec(
                "Starter domain",
                new StarterLexicon(),
                new StarterOntologyRegistry(),
                new StarterNonDialogTaskRegistry(),
                new StarterDatabaseRegistry()));
    }
    public static void main(String[] args) throws IOException {
        SubprocessYodaSystem.main(args);
    }
}


