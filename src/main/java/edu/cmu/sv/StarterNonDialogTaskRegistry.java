package edu.cmu.sv;

import edu.cmu.sv.domain.NonDialogTaskRegistry;

/**
 * Created by David Cohen on 6/23/15.
 */
public class StarterNonDialogTaskRegistry extends NonDialogTaskRegistry {
    public StarterNonDialogTaskRegistry() {
        // TODO: register non-dialog tasks and add action schema below
        /*
        nonDialogTasks.add(TurnOnTask.class);
        actionSchemata.add(new GenericCommandSchema(StarterOntologyRegistry.turnOnAppliance, TurnOnTask.class));
        */

    }
}

