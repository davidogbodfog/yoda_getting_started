package edu.cmu.sv;

import edu.cmu.sv.domain.DatabaseRegistry;

/**
 * Created by David Cohen on 6/23/15.
 */
public class StarterDatabaseRegistry extends DatabaseRegistry {
    public StarterDatabaseRegistry() {
        turtleDatabaseSources.add("./src/resources/starter.turtle");
    }
}

