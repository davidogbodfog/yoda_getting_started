package edu.cmu.sv;

import edu.cmu.sv.natural_language_generation.Lexicon;

/**
 * Created by David Cohen on 6/23/15.
 */
public class StarterLexicon extends Lexicon {
    public StarterLexicon() {
        //TODO: define lexicon below
        /*
        //// Lexicon for nouns
        {
            {
                LexicalEntry entry = new LexicalEntry();
                entry.add(LexicalEntry.PART_OF_SPEECH.SINGULAR_NOUN, "appliance");
                entry.add(LexicalEntry.PART_OF_SPEECH.PLURAL_NOUN, "appliances");
                add(SmartHouseOntologyRegistry.appliance, entry, false);
            }
        }

        //// Lexicon for prepositions
        {
            {
                LexicalEntry entry = new LexicalEntry();
                entry.add(LexicalEntry.PART_OF_SPEECH.PREPOSITION, "in");
                entry.add(LexicalEntry.PART_OF_SPEECH.PREPOSITION, "inside");
                entry.add(LexicalEntry.PART_OF_SPEECH.PREPOSITION, "at");
                add(SmartHouseOntologyRegistry.isContainedBy, entry, false);
            }
        }


            //// Lexicon for adjectives
        {
            {
                LexicalEntry entry = new LexicalEntry();
                entry.add(LexicalEntry.PART_OF_SPEECH.ADJECTIVE, "on");
                entry.add(LexicalEntry.PART_OF_SPEECH.ADJECTIVE, "turned on");
                add(SmartHouseOntologyRegistry.on, entry, false);
            }
        }

        //// Lexicon for qualities
        {
        }
        //// Lexicon for verbs
        {
            {
                LexicalEntry entry = new LexicalEntry();
                entry.add(LexicalEntry.PART_OF_SPEECH.S1_VERB, "turn on");
                entry.add(LexicalEntry.PART_OF_SPEECH.S1_VERB, "power on");
                entry.add(LexicalEntry.PART_OF_SPEECH.SINGULAR_NOUN, "turn on");
                entry.add(LexicalEntry.PART_OF_SPEECH.SINGULAR_NOUN, "power on");
                entry.add(LexicalEntry.PART_OF_SPEECH.ADJECTIVE, "on");
                add(SmartHouseOntologyRegistry.turnOnAppliance, entry, false);
            }
        }

        //// Lexicon for roles
        {
            {
                LexicalEntry entry = new LexicalEntry();
                entry.add(LexicalEntry.PART_OF_SPEECH.AS_OBJECT_PREFIX, "");
                add(SmartHouseOntologyRegistry.component, entry, false);
            }
        }
        */

    }

}
